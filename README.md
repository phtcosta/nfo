# NFO #

This is a basic library to read/write [NFO](http://kodi.wiki/view/NFO_files) files (movie information **only**)


## Basic Usage ##



```
#!java

import java.io.File;
import br.com.phtcosta.nfo.model.Movie;

//...

NfoParser parser = new NfoParser();

```

### Read NFO ###


```
#!java

Movie movie = parser.readNfo(nfoFile);

```

### Write NFO ###


```
#!java

parser.writeNfo(nfoFile, movie);

```