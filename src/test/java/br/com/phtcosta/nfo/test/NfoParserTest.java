package br.com.phtcosta.nfo.test;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import br.com.phtcosta.nfo.NfoParser;
import br.com.phtcosta.nfo.model.Movie;

public class NfoParserTest {

	@Test
	public void testReadNfo() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		File nfoFile = new File(classLoader.getResource("movie.nfo").getFile());

		assertTrue(nfoFile.exists());

		NfoParser parser = new NfoParser();
		Movie movie = parser.readNfo(nfoFile);

		assertNotNull(movie);
		assertEquals("The Silence of the Lambs", movie.getOriginaltitle());
		assertEquals(1991, movie.getYear());
		assertEquals("tt0102926", movie.getId());
		assertEquals(62, movie.getActor().size());
		assertEquals(5, movie.getProducer().size());
		assertEquals(3, movie.getGenre().size());
		assertEquals(2, movie.getCredits().size());		
	}

}
