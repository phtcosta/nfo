package br.com.phtcosta.nfo;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import br.com.phtcosta.nfo.model.Movie;

public class NfoParser {
	
	public void writeNfo(File out, Movie movie) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(Movie.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(movie, System.out);
		jaxbMarshaller.marshal(movie, out);
	}

	public Movie readNfo(File nfoFile) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(Movie.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		return (Movie) jaxbUnmarshaller.unmarshal(nfoFile);
	}

}
